package com.example.gauravvenkatesh.myapplication;

/**
 * Created by Gaurav Venkatesh on 5/11/2015.
 */

import android.content.Context;
import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by Gaurav Venkatesh on 5/9/2015.
 */
public class SuperMarioSuperView  extends SurfaceView implements SurfaceHolder.Callback{

    public  SuperMarioSuperView(Context context)
    {
        super(context);
        getHolder().addCallback(this);

    }
    @Override
    public  void  surfaceCreated(SurfaceHolder holder)
    {
        SuperMarioThread bsv = new SuperMarioThread(this);
        bsv.start();

    }
    public void surfaceChanged(SurfaceHolder holder,
                               int format, int width, int height)
    {

    }


    public void surfaceDestroyed(SurfaceHolder holder)
    {

    }


    @Override
    public  void  onDraw(Canvas c)
    {
        super.onDraw(c);

    }
    @Override
    public boolean onTouchEvent(MotionEvent e) {


        switch (e.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                // Update Game State.

                break;
            case MotionEvent.ACTION_MOVE:
                // Update Game State.
                break;
            case MotionEvent.ACTION_UP:
                // Update Game State.

        }
        return true;
    }


    public  void  tick(Canvas c) {


    }
}
